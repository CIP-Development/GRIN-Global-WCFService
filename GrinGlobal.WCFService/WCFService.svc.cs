﻿using GrinGlobal.Business;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace GrinGlobal.WCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WCFService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WCFService.svc or WCFService.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class WCFService : IWCFService
    {
        public Stream Getdata(string dataview, string parameters, string limit)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;

            var JSONString = string.Empty;
            try
            {
                var httpToken = headers["Authorization"].Trim().Replace("Bearer ", "");

                using (SecureData sd = new SecureData(false, httpToken))
                {
                    if (string.IsNullOrEmpty(limit)) limit = "0";
                    var dsResponse = sd.GetData(dataview, parameters, 0, int.Parse(limit), "");
                    if (dsResponse != null && dsResponse.Tables.Contains(dataview))
                    {
                        JSONString = JsonConvert.SerializeObject(dsResponse.Tables[dataview]);
                    }
                }
            }
            catch (Exception e)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError; //500
                JSONString = JsonConvert.SerializeObject(e.Message);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }

        public Status Login(Credential credential)
        {
            var status = new Status();
            try
            {
                var dsValidateLogin = SecureData.TestLogin(false, credential.Username, credential.Password);

                if (dsValidateLogin.Tables.Contains("validate_login") && dsValidateLogin.Tables["validate_login"].Rows.Count > 0)
                {
                    status.CooperatorId = dsValidateLogin.Tables["validate_login"].Rows[0].Field<int>("cooperator_id");
                    status.Token = SecureData.Login(credential.Username, credential.Password);
                    if (string.IsNullOrEmpty(status.Token))
                    {
                        status.Success = false;
                        status.Error = "Error generating token";
                    }
                    else
                    {
                        status.Success = true;
                    }
                }
                else
                {
                    status.Success = false;
                    status.Error = "Error validating credentials";
                }
            }
            catch (Exception ex)
            {
                status.Success = false;
                status.Error = ex.Message;
            }

            return status;
        }

        public Stream CreateData(string tablename, string data)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;
            var JSONString = string.Empty;

            try
            {
                var entity = JObject.Parse(data);
                var internalDataview = "table_mob_" + tablename;
                //var entity2 = JsonConvert.DeserializeObject(data);

                var httpToken = headers["Authorization"].Trim().Replace("Bearer ", "");
                using (SecureData sd = new SecureData(false, httpToken))
                {
                    var dataset = sd.GetData(internalDataview, ":" + tablename.Replace("_", "") + "id=0", 0, 0, "");
                    var table = dataset.Tables[internalDataview];
                    var row = table.NewRow();

                    foreach (DataColumn c in table.Columns)
                    {
                        JToken prop = entity[c.ColumnName];
                        if (prop != null && prop.Type != JTokenType.Null && !prop.HasValues)
                            row.SetField(c, prop);
                    }
                    table.Rows.Add(row);

                    DataSet dsChanges = new DataSet();
                    dsChanges.Tables.Add(table.GetChanges());
                    var dsError = sd.SaveData(dsChanges, true, "");
                    if (dsError.Tables.Contains(internalDataview))
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK; //200
                        JSONString = JsonConvert.SerializeObject(dsError.Tables[internalDataview].Rows[0]["NewPrimaryKeyID"]);

                        JToken workgroupCooperatorId = entity["workgroup_cooperator_id"];
                        var newPrimaryKeyID = int.Parse(JSONString);
                        if (workgroupCooperatorId != null && workgroupCooperatorId.Type != JTokenType.Null && !workgroupCooperatorId.HasValues)
                        {
                            try
                            {
                                using (Core.DataManager dm = sd.BeginProcessing(true))
                                {
                                    dm.BeginTran();

                                    var coopIDs = new List<int>
                                    {
                                        sd.CooperatorID
                                    };

                                    var dv = Dataview.Map(internalDataview, sd.LanguageID, dm);
                                    if (dv != null)
                                    {
                                        var pkNames = dv.PrimaryKeyNames;
                                        if (pkNames.Count > 0)
                                        {
                                            var pkName = pkNames[0];
                                            var tableName = dv.PrimaryKeyTableName;
                                            var newOwnerCooperatorID = workgroupCooperatorId.Value<int>();
                                            var validCooperatorIDs = coopIDs;
                                            foreach (DataRow dr in dsChanges.Tables[internalDataview].Rows)
                                            {
                                                var sqlUpdateOwner = @"
update
    {0}
set
    owned_by = :newowner,
    owned_date = :now
where
    owned_by in (:curowner)
    and {1} = :pkvalue
";
                                                dm.Write(String.Format(sqlUpdateOwner, tableName, pkName), new Core.DataParameters(
                                                    ":newowner", newOwnerCooperatorID, DbType.Int32,
                                                    ":now", DateTime.UtcNow, DbType.DateTime2,
                                                    ":curowner", validCooperatorIDs, Core.DbPseudoType.IntegerCollection,
                                                    ":pkvalue", newPrimaryKeyID, DbType.Int32));
                                            }
                                        }
                                    }

                                    dm.Commit();
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Error at setting workgroup cooperator" + Environment.NewLine + ex.Message);
                            }
                        }
                    }
                    else
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError; //500
                        JSONString = JsonConvert.SerializeObject(dsError.Tables["ExceptionTable"]);
                    }
                }
            }
            catch (Exception e)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError; //500
                JSONString = JsonConvert.SerializeObject(e.Message);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }

        public Stream ReadData(string tablename, string id)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;

            var JSONString = string.Empty;
            try
            {
                var httpToken = headers["Authorization"].Trim().Replace("Bearer ", "");
                using (SecureData sd = new SecureData(false, httpToken))
                {
                    var dataset = sd.GetData("table_mob_" + tablename, ":" + tablename.Replace("_", "") + "id=" + id, 0, 0, "");
                    var table = dataset.Tables["table_mob_" + tablename];
                    if (dataset.Tables["table_mob_" + tablename].Rows.Count > 0)
                    {
                        var row = table.Rows[0];
                        var entity = new Dictionary<string, object>();
                        foreach (DataColumn c in table.Columns)
                        {
                            entity.Add(c.ColumnName, row.Field<object>(c.ColumnName));
                        }
                        WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK; //200
                        JSONString = JsonConvert.SerializeObject(entity);
                    }
                    else
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                        JSONString = JsonConvert.SerializeObject("Resource Not Found");
                    }
                }
            }
            catch (Exception e)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError; //500
                JSONString = JsonConvert.SerializeObject(e.Message);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }

        public Stream UpdateData(string tablename, string id, string data)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;
            var JSONString = string.Empty;

            try
            {
                var entity = JObject.Parse(data);

                var httpToken = headers["Authorization"].Trim().Replace("Bearer ", "");
                using (SecureData sd = new SecureData(false, httpToken))
                {
                    var dataset = sd.GetData("table_mob_" + tablename, ":" + tablename.Replace("_", "") + "id=" + id, 0, 0, "");

                    if (dataset.Tables["table_mob_" + tablename].Rows.Count > 0)
                    {
                        var table = dataset.Tables["table_mob_" + tablename];
                        var row = table.Rows[0];

                        foreach (DataColumn c in table.Columns)
                        {
                            JToken prop = entity[c.ColumnName];
                            if (prop != null && !c.ReadOnly)
                            {
                                if (prop.Type == JTokenType.Null)
                                {
                                    row[c] = DBNull.Value;
                                }
                                else
                                {
                                    row.SetField(c, prop);
                                }
                            }
                        }

                        var dsChanges = new DataSet();
                        dsChanges.Tables.Add(table.GetChanges());
                        var dsError = sd.SaveData(dsChanges, true, "");

                        if (dsError.Tables.Contains("table_mob_" + tablename))
                        {
                            WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK; //200
                            JSONString = string.Empty;
                            /*if (dsError.Tables["table_mob_" + tablename].Rows.Count > 0)
                                JSONString = JsonConvert.SerializeObject(dsError.Tables["table_mob_" + tablename].Rows[0]);
                            */
                        }
                        else
                        {
                            WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                            JSONString = JsonConvert.SerializeObject(dsError.Tables["ExceptionTable"]);
                        }
                    }
                    else
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                        JSONString = JsonConvert.SerializeObject("Resource Not Found");
                    }
                }
            }
            catch (Exception e)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError; //500
                JSONString = JsonConvert.SerializeObject(e.Message);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }

        public Stream DeleteData(string tablename, string id)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;
            var JSONString = string.Empty;

            try
            {
                var httpToken = headers["Authorization"].Trim().Replace("Bearer ", "");
                using (SecureData sd = new SecureData(false, httpToken))
                {
                    var dataset = sd.GetData("table_mob_" + tablename, ":" + tablename.Replace("_", "") + "id=" + id, 0, 0, "");

                    if (dataset.Tables["table_mob_" + tablename].Rows.Count > 0)
                    {
                        var table = dataset.Tables["table_mob_" + tablename];
                        var nr = table.Rows[0];
                        nr.Delete();

                        var dsError = sd.SaveData(dataset, true, "");
                        if (dsError.Tables.Contains("table_mob_" + tablename))
                        {
                            WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                            JSONString = string.Empty;
                        }
                        else
                        {
                            WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                            JSONString = JsonConvert.SerializeObject(dsError.Tables["ExceptionTable"]);
                        }
                    }
                    else
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                        JSONString = JsonConvert.SerializeObject("Resource Not Found");
                    }
                }
            }
            catch (Exception e)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError; //500
                JSONString = JsonConvert.SerializeObject(e.Message);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }

        public Stream SearchData(string tablename, string dataview, string query, string limit, string offset)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;

            var JSONString = string.Empty;
            try
            {
                var httpToken = headers["Authorization"].Trim().Replace("Bearer ", "");

                using (SecureData sd = new SecureData(false, httpToken))
                {
                    if (string.IsNullOrEmpty(offset)) offset = "0";
                    if (string.IsNullOrEmpty(limit)) limit = "0";
                    //sd.Search(query, ignoreCase, andTermsTogether, indexList, resolverName, offset, limit, 0, 0, null, options, null, null)
                    DataSet ds = sd.Search(query, true, true, null, tablename, int.Parse(offset), int.Parse(limit), 0, 0, null, "", null, null);

                    if (ds.Tables["SearchResult"].Rows.Count > 0)
                    {
                        var ids = new List<string>();
                        foreach (DataRow row in ds.Tables["SearchResult"].Rows)
                        {
                            ids.Add(row.ItemArray[0].ToString());
                        }
                        if (string.IsNullOrEmpty(dataview))
                        {
                            var response = sd.GetData("table_mob_" + tablename, ":" + tablename.Replace("_", "") + "id=" + string.Join(",", ids.ToArray()), int.Parse(offset), int.Parse(limit), "");
                            JSONString = JsonConvert.SerializeObject(response.Tables["table_mob_" + tablename]);
                        }
                        else
                        {
                            var response = sd.GetData(dataview, ":" + tablename.Replace("_", "") + "id= " + string.Join(",", ids.ToArray()), int.Parse(offset), int.Parse(limit), "");
                            JSONString = JsonConvert.SerializeObject(response.Tables[dataview]);
                        }
                    }
                    else
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NoContent; //204
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError; //500
                JSONString = JsonConvert.SerializeObject(e.Message);
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }

        public Stream SearchKeys(string tablename, string query, string limit, string offset)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;

            var JSONString = string.Empty;
            try
            {
                var httpToken = headers["Authorization"].Trim().Replace("Bearer ", "");

                using (SecureData sd = new SecureData(false, httpToken))
                {
                    if (string.IsNullOrEmpty(offset)) offset = "0";
                    if (string.IsNullOrEmpty(limit)) limit = "0";
                    //sd.Search(query, ignoreCase, andTermsTogether, indexList, resolverName, offset, limit, 0, 0, null, options, null, null)
                    DataSet ds = sd.Search(query, true, true, null, tablename, int.Parse(offset), int.Parse(limit), 0, 0, null, "", null, null);

                    if (ds.Tables["SearchResult"].Rows.Count > 0)
                    {
                        var ids = new List<int>();
                        foreach (DataRow row in ds.Tables["SearchResult"].Rows)
                        {
                            ids.Add(row.Field<int>(0));
                        }
                        JSONString = JsonConvert.SerializeObject(ids);
                    }
                    else
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NoContent; //204
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError; //500
                JSONString = JsonConvert.SerializeObject(e.Message);
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }

        public Stream Print(string printURI, string printConnectionType, string zpl)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;

            var JSONString = string.Empty;
            try
            {
                var httpToken = headers["Authorization"].Trim().Replace("Bearer ", "");
                using (SecureData sd = new SecureData(false, httpToken))
                {
                    if (printConnectionType.Equals("Shared"))
                    {
                        //var FILE_NAME = System.Web.Hosting.HostingEnvironment.MapPath("~/ZPLII.txt");
                        var FILE_NAME = System.Web.Hosting.HostingEnvironment.MapPath("~/uploads/ZPLII.txt");

                        File.WriteAllText(FILE_NAME, zpl);
                        File.Copy(FILE_NAME, printURI);
                    }
                    else if (printConnectionType.Equals("IP"))
                    {
                        const int port = 9100;

                        using (System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient())
                        {
                            client.Connect(printURI, port);

                            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(client.GetStream()))
                            {
                                writer.Write(zpl);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError; //500
                JSONString = JsonConvert.SerializeObject(e.Message);
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }

        public Stream ChangePassword(Credential credential)
        {
            var JSONString = string.Empty;
            try
            {
                SecureData.ChangePassword(false, credential.Username, credential.Password, credential.Username, credential.NewPassword);
            }
            catch (Exception ex)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError; //500
                JSONString = JsonConvert.SerializeObject(ex.Message);
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }

        public Stream ClearCache(string token)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;

            var JSONString = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(token))
                {
                    token = headers["Authorization"].Trim().Replace("Bearer ", "");
                }
                using (SecureData sd = new SecureData(false, token))
                {
                    var isOk = sd.ClearCache(string.Empty, false);
                    if (isOk)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK; //200
                        JSONString = JsonConvert.SerializeObject("Cache cleared");
                    }
                    else
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.BadRequest;
                        JSONString = JsonConvert.SerializeObject("ClearCache returns false");
                    }
                }
            }
            catch (Exception ex)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError; //500
                JSONString = JsonConvert.SerializeObject(ex.Message);
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }
    }
}
